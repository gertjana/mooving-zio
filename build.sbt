scalaVersion := "2.12.16"
organization := "dev.ga"
name         := "mooving-zio"

val zioVersion = "2.0.0"
val tapirVersion = "1.0.3"
libraryDependencies ++= Seq(
  "dev.zio"                     %% "zio"                    % zioVersion,
  "dev.zio"                     %% "zio-json"               % "0.3.0-RC7",
  "io.d11"                      %% "zhttp"                  % "2.0.0-RC7",
  "com.faunadb"                 %% "faunadb-scala"          % "5.0.0-beta",
  "com.softwaremill.sttp.tapir" %% "tapir-zio-http-server"  % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-json-zio"         % tapirVersion,
  "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % tapirVersion,
  "dev.zio"                     %% "zio-test"               % zioVersion % "test",
  "dev.zio"                     %% "zio-test-sbt"           % zioVersion % "test",
  "dev.zio"                     %% "zio-test-junit"         % zioVersion % "test",
  "com.softwaremill.sttp.tapir" %% "tapir-sttp-stub-server" % "1.0.5" % "test"
)

scalacOptions ++= Seq(
  "-encoding", "utf8",
//  "-Xfatal-warnings",
  "-deprecation",
  "-unchecked",
  "-feature",
  "-language:implicitConversions",
  "-language:higherKinds",
  "-language:existentials",
  "-language:postfixOps"
)

testFrameworks += new TestFramework("zio.test.sbt.ZTestframework")
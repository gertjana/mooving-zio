# Mooving ZIO

Trying to replicate my mooving app (Exists already for Elixir and Go) in Scala with ZIO

I tend to do this whenever I want to learn more about a certain technology, in this case [zio](https://zio.dev) and [faunadb](https://faunadb.com)

A Moovable is a vehicle that tracks how many units (km/steps/lightyears) it has 'moved' 

This application allows you to store this information and linearly extrapolate a 'trend'

It uses
- [ZIO 2.0](https://zio.dev)
- [ZIO HTTP](https://dream11.github.io/zio-http/) for the HTTP server
- [ZIO JSON](https://zio.github.io/zio-json/) for the JSON serialization
- [Fauna DB](https://faunadb.com) for persistence

## Running The app

```bash
FAUNA_SECRET="your_fauna_secret" sbt run
```

## endpoints

| action             | method | endpoint                                                 | body                                                                                     |
|--------------------|--------|----------------------------------------------------------|------------------------------------------------------------------------------------------|
| get a list         | GET    | /moovable?type=&lt;type>                                 |                                                                                          |
| get a single       | GET    | /moovable/&lt;name>                                      |                                                                                          |
| add a new moovable | POST   | /moovable                                                | {"name": "my_bike", "type": "bike", "current": false, "unit": "km", "seq": 1, "data":[]} |
| add a data point   | PATCH  | /moovable/&lt;name>                                      | {"date": "2022-07-26", "value": "12345"}                                                 |
| get the trend      | GET    | /moovable/&lt;name>/trend?for_date=&lt;some future date> |                                                                                          |

package dev.ga.mooving

import dev.ga.mooving.moovable.MoovableApp
import dev.ga.mooving.moovable.calculations.TrendService
import dev.ga.mooving.moovable.fauna.FaunaMoovableRepo
import dev.ga.mooving.moovable.inmemory.InmemoryMoovableRepo
import zhttp.service.Server
import zio._

import java.net.InetAddress

object MainApp extends ZIOAppDefault {
  def run: ZIO[Any, Throwable, Nothing] =
    (for {
      address <- System.envOrElse("ADDRESS", "0.0.0.0")
      port <- System.envOrElse("PORT", "8080")
      _ <- Console.printLine(s"Server started on $address:$port")
      server <- Server.start(
        address = InetAddress.getByName(address),
        port = port.toInt,
        http = MoovableApp()
      )
    } yield server).provide(
//        InmemoryMoovableRepo.live,
        FaunaMoovableRepo.live,
        TrendService.live,
//        ZLayer.Debug.tree
      )
}




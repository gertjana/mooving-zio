package dev.ga.mooving.moovable

import faunadb.values.Codec
import zio.json._

import java.time.LocalDate

case class Data(date: LocalDate, value: Int)

object Data {
  // for Fauna
  implicit val DataCodec: Codec[Data] = Codec.Record[Data]

  // for ZIO HTTP
  implicit val encoder: JsonEncoder[Data] = DeriveJsonEncoder.gen[Data]
  implicit val decoder: JsonDecoder[Data] = DeriveJsonDecoder.gen[Data]

  def emptyList: List[Data] = List[Data]()
}

case class Trend(total: Double = 0.0, trend: Double = 0.0, dailyAvg: Double = 0.0)

object Trend {
  // for Fauna
  implicit val DataCodec: Codec[Trend] = Codec.Record[Trend]

  // for ZIO HTTP
  implicit val encoder: JsonEncoder[Trend] = DeriveJsonEncoder.gen[Trend]
  implicit val decoder: JsonDecoder[Trend] = DeriveJsonDecoder.gen[Trend]

  def empty: Trend = Trend()

}


case class Moovable(id: Option[String] = None, name: String, `type`: String, current: Option[Boolean] = None, unit: Option[String] = None, seq: Int, data: List[Data])

object Moovable {
  // for Fauna
  implicit val MoovableCodec: Codec[Moovable] = Codec.Record[Moovable]
  
  // for ZIO HTTPa
  implicit val encoder: JsonEncoder[Moovable] = DeriveJsonEncoder.gen[Moovable]
  implicit val decoder: JsonDecoder[Moovable] = DeriveJsonDecoder.gen[Moovable]
}
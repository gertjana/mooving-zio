package dev.ga.mooving.moovable

import sttp.tapir.{Endpoint, EndpointInput, PublicEndpoint}
import sttp.tapir.generic.auto._
import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.json.zio._
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import sttp.tapir.ztapir._

import zhttp.http._
import zio._

object MoovableApp {
  def apply(): HttpApp[MoovableRepo, Throwable] = {

    // endpoints for swagger
    val endpoints = List(getMoovableEndpoint, listMoovablesEndpoint, getMoovableTrendEndpoint, patchMoovableDataEndpoint, postNewMoovableEndpoint)

    ZioHttpInterpreter().toHttp(listMoovablesEndpoint.zServerLogic(listMoovables)) ++
      ZioHttpInterpreter().toHttp(getMoovableEndpoint.zServerLogic(getMoovable)) ++
      ZioHttpInterpreter().toHttp(getMoovableTrendEndpoint.zServerLogic(getMoovableTrend)) ++
      ZioHttpInterpreter().toHttp(patchMoovableDataEndpoint.zServerLogic(patchMoovableData)) ++
      ZioHttpInterpreter().toHttp(postNewMoovableEndpoint.zServerLogic(postMoovable)) ++
      ZioHttpInterpreter().toHttp(SwaggerInterpreter().fromEndpoints[Task](endpoints, "Mooving", "1.0"))
  }

  // Endpoints
  private val baseEndpoint: Endpoint[Unit, Unit, Unit, Unit, Any] = endpoint.in("moovable")
  private val baseGetEndpoint: Endpoint[Unit, Unit, Unit, Unit, Any] = baseEndpoint.get
  private val basePatchEndpoint: Endpoint[Unit, Unit, Unit, Unit, Any] = baseEndpoint.patch
  private val basePostEndpoint: Endpoint[Unit, Unit, Unit, Unit, Any] = baseEndpoint.post

  private val namePath: EndpointInput.PathCapture[String] = path[String]("name")

  private val hideDataOptParam: EndpointInput.Query[Option[String]] = query[Option[String]]("hide_data")
  private val typeOptParam: EndpointInput.Query[Option[String]] = query[Option[String]]("type")
  private val forDateParam: EndpointInput.Query[String] = query[String]("for_date")

  val listMoovablesEndpoint: PublicEndpoint[(Option[String], Option[String]), String, List[Moovable], Any] =
    baseGetEndpoint
      .in(typeOptParam.and(hideDataOptParam))
      .out(jsonBody[List[Moovable]])
      .errorOut(stringBody)

  val getMoovableEndpoint: PublicEndpoint[(String, Option[String]), String, Moovable, Any] =
    baseGetEndpoint
      .in(namePath.and(hideDataOptParam))
      .out(jsonBody[Moovable])
      .errorOut(stringBody)

  val getMoovableTrendEndpoint: PublicEndpoint[(String, String, Option[String]), String, (Moovable, Trend), Any] =
    baseGetEndpoint
      .in(namePath / "trend").in(forDateParam.and(hideDataOptParam))
      .out(jsonBody[(Moovable, Trend)])
      .errorOut(stringBody)

  val patchMoovableDataEndpoint: PublicEndpoint[(String, Data), String, Moovable, Any] =
    basePatchEndpoint
      .in(namePath).in(jsonBody[Data])
      .out(jsonBody[Moovable])
      .errorOut(stringBody)

  val postNewMoovableEndpoint: Endpoint[Unit, Moovable, String, Moovable, Any] =
    basePostEndpoint
      .in(jsonBody[Moovable])
      .out(jsonBody[Moovable])
      .errorOut(stringBody)

  // Implementations
  def listMoovables(params: (Option[String], Option[String])): ZIO[MoovableRepo, String, List[Moovable]] = {
    val moovable_type: String = params._1.getOrElse("all")
    val hide_data: Boolean = params._2.nonEmpty
    MoovableRepo.list(moovable_type, hide_data).mapError(_.getMessage)
  }

  private def getMoovable(params: (String, Option[String])): ZIO[MoovableRepo, String, Moovable] = {
    val name = params._1
    val hide_data = params._2.nonEmpty
    MoovableRepo.get(name, hide_data).mapError(_.getMessage)
  }

  private def getMoovableTrend(params: (String, String, Option[String])): ZIO[MoovableRepo, String, (Moovable, Trend)] = {
    val name = params._1
    val for_date = params._2
    val hide_data = params._3.nonEmpty
    MoovableRepo.trend(name, for_date, hide_data)
  }

  private def patchMoovableData(params: (String, Data)): ZIO[MoovableRepo, String, Moovable] = {
    val name = params._1
    val data = params._2
    MoovableRepo.add_data(name, data).mapError(_.getMessage)
  }

  private def postMoovable(moovable: Moovable): ZIO[MoovableRepo, String, Moovable] = {
    MoovableRepo.add(moovable).mapError(_.getMessage)
  }
}


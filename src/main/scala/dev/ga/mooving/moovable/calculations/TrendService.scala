package dev.ga.mooving.moovable.calculations

import dev.ga.mooving.moovable.{Data, Moovable, Trend}
import zio._

import java.time.LocalDate
import java.time.temporal.ChronoUnit.DAYS
import scala.annotation.tailrec

trait TrendService {
  def calculate(moovable: Moovable, for_date: String): IO[String, Trend]
}

object TrendService {
  def live: ZLayer[Any, Nothing, TrendService] =
    ZLayer.scoped(ZIO.succeed(new TrendImpl()))
}

class TrendImpl() extends TrendService {
  override def calculate(moovable: Moovable, future_date: String): IO[String,Trend] = {
    for {
      date <- ZIO.attempt(LocalDate.parse(future_date)).mapError(_.getMessage())
      trend <-  ZIO.succeed(calcTrend(moovable, date))
    } yield trend
  }

  private def calcTrend(moovable: Moovable, for_date: LocalDate): Trend = {
    if (moovable.data.size > 1) {
      val sorted_data = moovable.data.sortBy(_.value)(Ordering[Int].reverse)
      val averages: Seq[Double] = calc_averages(List[Double](), None, moovable.data)
      val total: Double = sorted_data.head.value
      val daily_avg: Double = averages.fold(0.0)(_+_) / averages.size
      val trend: Double = total + (daily_avg * DAYS.between(sorted_data.head.date, for_date).doubleValue())
      Trend(total, tr(trend), tr(daily_avg))
    } else {
      Trend.empty
    }
  }

  private def average(data1: Data, data2: Data): Double =
    (data2.value - data1.value).toDouble / DAYS.between(data1.date, data2.date)

  @tailrec
  private def calc_averages(result: List[Double], last: Option[Data], values: List[Data]): List[Double] =
    values.reverse match {
      case head :: tail if last.isEmpty => calc_averages(result, Some(head), tail)
      case head :: tail => calc_averages(average(last.get, head) :: result, Some(head), tail)
      case _ => result
      }

  private def tr(n: Double): Double = (math rint n * 100) / 100

}


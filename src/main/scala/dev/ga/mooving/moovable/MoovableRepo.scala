package dev.ga.mooving.moovable

import zio._

trait MoovableRepo {
  def list(moovable_type: String, hide_data: Boolean): Task[List[Moovable]]

  def get(name: String, hide_data: Boolean): Task[Moovable]

  def add_data(name: String, data: Data): Task[Moovable]

  def add(moovable: Moovable): Task[Moovable]

  def trend(name: String, for_date: String, hide_data: Boolean): IO[String, (Moovable, Trend)]
}

object MoovableRepo {
  def  list(moovable_type: String, hide_data: Boolean): ZIO[MoovableRepo, Throwable, List[Moovable]] =
    ZIO.serviceWithZIO[MoovableRepo](_.list(moovable_type, hide_data))

  def get(id: String, hide_data: Boolean): ZIO[MoovableRepo, Throwable, Moovable] =
    ZIO.serviceWithZIO[MoovableRepo](_.get(id, hide_data))

  def add_data(name: String, data: Data): ZIO[MoovableRepo, Throwable, Moovable] = 
    ZIO.serviceWithZIO[MoovableRepo](_.add_data(name, data))

  def add(moovable: Moovable): ZIO[MoovableRepo, Throwable, Moovable] = 
    ZIO.serviceWithZIO[MoovableRepo](_.add(moovable))

  def trend(name: String, for_date: String, hide_data: Boolean): ZIO[MoovableRepo, String, (Moovable, Trend)] =
    ZIO.serviceWithZIO[MoovableRepo](_.trend(name, for_date, hide_data = hide_data))
}
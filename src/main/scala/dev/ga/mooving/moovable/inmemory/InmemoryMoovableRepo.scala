package dev.ga.mooving.moovable.inmemory

import dev.ga.mooving.moovable.calculations.{TrendImpl, TrendService}
import dev.ga.mooving.moovable.{Data, Moovable, MoovableRepo, Trend}
import zio._

import scala.collection.mutable

case class InmemoryMoovableRepo(trendService: TrendService, store: Ref[mutable.Map[String, Moovable]]) extends MoovableRepo {
  def add_data(name: String, data: Data): Task[Moovable] = for {
      moovable <- get(name, hide_data = false)
      new_moovable = moovable.copy(data = data +: moovable.data)
      _ <- store.updateAndGet(m => {m.update(moovable.name, new_moovable);m})
    } yield new_moovable

  def list(moovable_type: String, hide_data: Boolean): Task[List[Moovable]] = for {
      result <- store.get.map(_.values.toList)
    } yield result

  def get(name: String, hide_data: Boolean): Task[Moovable] = for {
      result <- store.get.map(_ (name))
    } yield result

  def add(moovable: Moovable): UIO[Moovable] = for {
    result <- store.updateAndGet(m => m+((moovable.name, moovable))).map(_(moovable.name))
  } yield result

  def trend(name: String, for_date:String, hide_data: Boolean): IO[String, (Moovable, Trend)] = for {
      moovable <- get(name, hide_data = true).mapError(cause => cause.getMessage)
      trend <- trendService.calculate(moovable, for_date)
    } yield (moovable, trend)
}

object InmemoryMoovableRepo {
    def live: ZLayer[Any, Nothing, InmemoryMoovableRepo] =
      ZLayer.fromZIO(
        Ref.make(mutable.Map.empty[String, Moovable]).map(new InmemoryMoovableRepo(new TrendImpl(), _))
      )
}


package dev.ga.mooving.moovable.fauna

import dev.ga.mooving.moovable.{Data, Moovable, MoovableRepo, Trend}
import dev.ga.mooving.moovable.calculations.{TrendImpl, TrendService}
import faunadb.FaunaClient
import faunadb.values.{RefV, Value}
import zio._

class FaunaMoovableRepo(trendService: TrendService, client: FaunaClient) extends MoovableRepo {
  import FaunaMoovableRepo._

  def list(moovable_type: String, hide_data: Boolean): Task[List[Moovable]] =
    ZIO.fromFuture(implicit ec => {
      client.query(Queries.list(moovable_type))
        .map(faunaToMoovables)
        .map(_.map(hideData(_, hide_data)))
    })

  def get(name: String, hide_data: Boolean): Task[Moovable] =
    ZIO.fromFuture(implicit ec => {
      client.query(Queries.get(name))
        .map(faunaToMoovables(_).head)
        .map(hideData(_, hide_data))
    })

  def trend(name: String, for_date: String, hide_data: Boolean): IO[String, (Moovable, Trend)] = {
    for {
      moovable <- get(name, hide_data = false).mapError(cause => cause.getMessage)
      trend <- trendService.calculate(moovable, for_date)
      new_moovable = hideData(moovable, hide_data)
    } yield (new_moovable, trend)
  }

  def add_data(name: String, data: Data): Task[Moovable] =  {
    for {
      m <- get(name, hide_data = false)
      nm <- ZIO.fromFuture(implicit ec => {
        val to_update = m.copy(data = data +: m.data, id = None)
        client.query(Queries.add_data(m.id.get, to_update))
          .map(faunaToMoovable)
      })
    } yield nm
  }
  
  def add(moovable: Moovable): Task[Moovable] = {
    for {
      _ <- ZIO.fromFuture(implicit ec => {
             client.query(Queries.add(moovable))
           }).orDie
      moovable <- get(moovable.name, hide_data = false)
    } yield moovable
  }
}

object FaunaMoovableRepo {
  def live: ZLayer[TrendService, Throwable, FaunaMoovableRepo] =
    ZLayer.scoped {
      for {
        fauna_secret <- System.envOrElse(variable = "FAUNA_SECRET", alt = "")
        fauna_endpoint <- System.envOrElse(variable = f"FAUNA_ENDPOINT",alt = "https://db.eu.fauna.com")
        client <- ZIO.attempt(
          FaunaClient(
            secret   = fauna_secret,
            endpoint = fauna_endpoint
          )
        ).map(c => new FaunaMoovableRepo(new TrendImpl(), c)
        )
      } yield client
    }

  private def hideData(moovable: Moovable, hide_data: Boolean): Moovable =
    if (hide_data) {
      moovable.copy(data = Data.emptyList)
    } else {
      moovable
    }

  private def faunaToMoovables(v: Value): List[Moovable] = v("data").get
    .to[Seq[Value]].get
    .map(faunaToMoovable)
    .toList

  private def faunaToMoovable(v: Value): Moovable = v("data")
    .to[Moovable].get
    .copy(id = Some(v("ref").get.to[RefV].get.id))
}


package dev.ga.mooving.moovable.fauna

import dev.ga.mooving.moovable.Moovable
import faunadb.query._
object Queries {
  def get(name: String): Expr = {
    Map(
      Paginate(Match(Index("moovables_by_name"), name)),
      Lambda("ref", Get(Var("ref")))
    )
  }

  def list(moovable_type: String): Expr = {
    moovable_type match {
      case "all" => 
        Map(
          Paginate(Match(Index("moovables_all_desc"))),
          Lambda(Array[String]("type","seq","ref"), Get(Var("ref")))
        )
      case _ => 
        Map(
          Paginate(Match(Index("moovables_by_type_desc"), moovable_type)),
          Lambda(Array[String]("seq", "ref"), Get(Var("ref")))
        )      
    }
  }

  def add_data(id: String, moovable: Moovable): Expr = {
    Update(
      Ref(Collection("Moovables"), id), 
      Obj("data" -> moovable)
    )
  }
  
  def add(moovable: Moovable): Expr = {
    Create(
      Collection("Moovables"),
      Obj("data" -> moovable)
    )
  }
}

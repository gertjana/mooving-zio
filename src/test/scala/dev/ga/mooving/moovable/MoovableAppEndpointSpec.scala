package dev.ga.mooving.moovable

import dev.ga.mooving.moovable.MoovableApp._
import sttp.client3._
import sttp.client3.testing.SttpBackendStub
import sttp.tapir.server.stub.TapirStubInterpreter
import zio.test.junit.JUnitRunnableSpec
import zio._
import zio.json._
import zio.test._

import java.time.LocalDate
import scala.concurrent.Future
import scala.util.Success

object MoovableAppEndpointSpec extends JUnitRunnableSpec {
  val moovable = Moovable(
    name = "testMoovable",
    `type` = "test",
    data = List[Data](),
    current = Some(false),
    unit = Some("km"),
    seq = 1
  )

  val moovable2 = moovable.copy(
    name = moovable.name + "2",
    data = List[Data](
      Data(date = LocalDate.parse("2022-01-01"), value = 0),
      Data(date = LocalDate.parse("2022-01-02"), value = 10),
      Data(date = LocalDate.parse("2022-01-03"), value = 20)
    )
  )

  def spec = suite("MoovableEndpointsSuite")(
    test("List Moovables Endpoint") {
      val backendStub: SttpBackend[Future, Any] = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
        .whenEndpoint(listMoovablesEndpoint)
        .thenRespond(List[Moovable](moovable, moovable2))
        .backend()
      val response = ZIO.fromFuture(implicit ec => {basicRequest
        .get(uri"https://test.moovables.com/moovable?type=all")
        .send(backendStub)})
      val objs: ZIO[Any, Throwable, List[Moovable]] = response.map(resp => resp.body.flatMap(_.fromJson[List[Moovable]]).right.get)

      assertZIO(response.map(_.code.code))(Assertion.equalTo(200))
      assertZIO(objs.map(_.size))(Assertion.equalTo(2))
      assertZIO(objs.map(_.map(_.name)))(Assertion.equalTo(List[String](moovable.name, moovable2.name)))
    },
    test("Add new Moovable Endpoint") {
      val backendStub: SttpBackend[Future, Any] = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
        .whenEndpoint(postNewMoovableEndpoint)
        .thenRespond(moovable)
        .backend()

      val response = ZIO.fromFuture(implicit ec => basicRequest
        .post(uri"https://test.moovables.com/moovable")
        .body(moovable.toJson)
        .send(backendStub))
      val obj: ZIO[Any, Throwable, Moovable] = response.map(_.body.flatMap(_.fromJson[Moovable]).right.get)

      assertZIO(response.map(_.code.code))(Assertion.equalTo(200))
      assertZIO(obj.map(_.name))(Assertion.equalTo(moovable.name))

    },
    test("Get Moovable") {
      val backendStub: SttpBackend[Future, Any] = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
        .whenEndpoint(getMoovableEndpoint)
        .thenRespond(moovable)
        .backend()

      val response = ZIO.fromFuture(implicit ec => basicRequest
        .get(uri"https://test.moovables.com/moovable/testmoovable")
        .send(backendStub))

      val obj: ZIO[Any, Throwable, Moovable] = response.map(resp => resp.body.flatMap(_.fromJson[Moovable]).right.get)
      val statusCode = response.map(_.code.code)

      assertZIO(statusCode)(Assertion.equalTo(200))
      assertZIO(obj)(Assertion.equalTo(moovable))
    },
    test("Get not-existing Moovable") {
      val backendStub: SttpBackend[Future, Any] = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
        .whenEndpoint(getMoovableEndpoint)
        .thenRespondError("testmoovable2 not found")
        .backend()

      val response = ZIO.fromFuture(implicit ec => basicRequest
        .get(uri"https://test.moovables.com/moovable/testmoovable2")
        .send(backendStub))

      val obj: ZIO[Any, Throwable, Moovable] = response.map(resp => resp.body.flatMap(_.fromJson[Moovable]).right.get)
      val statusCode = response.map(_.code.code)


      assertZIO(statusCode)(Assertion.equalTo(400))
    },
    test("Add Moovable Data") {
      val data = Data(date = LocalDate.parse("2022-08-12"), value = 1234)
      val moovable_plus = moovable.copy(data = data +: moovable.data)
      val backendStub: SttpBackend[Future, Any ] = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
        .whenEndpoint(patchMoovableDataEndpoint)
        .thenRespond(moovable2)
        .backend()

      val response = ZIO.fromFuture(implicit ec => basicRequest
        .patch(uri"https://test.moovables.com/moovable/testmoovable2")
        .body(data.toJson)
        .send(backendStub))

      val body = response.map(_.body.right.get)
      val statusCode = response.map(_.code.code)

      assertZIO(body)(Assertion.equalTo(moovable_plus.toJson))
      assertZIO(statusCode)(Assertion.equalTo(200))
    },
    test("Moovable Trend") {
      val backendStub: SttpBackend[Future, Any] = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
        .whenEndpoint(getMoovableTrendEndpoint)
        .thenRespond((moovable, Trend.empty))
        .backend()

      val response = ZIO.fromFuture( implicit ec => {basicRequest
        .get(uri"https://test.moovables.com/moovable/testmoovable/trend?for_date=2022-12-31&hide_data=true")
        .send(backendStub)
      })

      val message: ZIO[Any, Throwable, String] = response.map(_.body.right.get)
      val statusCode: ZIO[Any, Throwable, Int] = response.map(_.code.code)

      assertZIO(statusCode)(Assertion.equalTo(200))
      assertZIO(message)(Assertion.equalTo(((moovable, Trend.empty).toJson)))
    },

  )



}

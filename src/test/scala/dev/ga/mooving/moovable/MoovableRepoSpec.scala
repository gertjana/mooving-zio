package dev.ga.mooving.moovable

import zio._
import zio.test.TestAspect.{parallel, sequential, timed}
import zio.test._
import zio.test.Assertion._
import zio.test.junit.JUnitRunnableSpec

import java.time.LocalDate

object MoovableRepoSpec extends JUnitRunnableSpec{
  val moovable = Moovable(
    name = "TestMoovable",
    `type` = "test",
    data = List[Data](),
    current = Some(false),
    unit = Some("km"),
    seq = 1
  )

  val moovable2 = moovable.copy(
    name = moovable.name + "2",
    data = List[Data](
      Data(date = LocalDate.parse("2022-01-01"), value = 0),
      Data(date = LocalDate.parse("2022-01-02"), value = 10),
      Data(date = LocalDate.parse("2022-01-03"), value = 20)
    )
  )


  def spec = suite("MoovableRepo Suite")(
    test("Test create Moovable") {
      val added = MoovableRepo.add(moovable)
      assertZIO(added.map(_.name).orDie)(Assertion.equalTo(moovable.name))
    },
    test("Test create another Moovable") {
      val added = MoovableRepo.add(moovable2)
      assertZIO(added.map(_.name).orDie)(Assertion.equalTo(moovable2.name))
    },
    test("Test get Moovable") {
      val m = MoovableRepo.get("TestMoovable", hide_data = true)
      assertZIO(m.map(_.name).orDie)(Assertion.equalTo(moovable.name))
    },
    test("Test list Moovable"){
      val xs = MoovableRepo.list("test", hide_data = true)
      assertZIO(xs.map(_.size).orDie)(Assertion.equalTo(2))
    },
    test("Test add_data Moovable"){
      val result = MoovableRepo.add_data(moovable.name, Data(value = 123, date = LocalDate.parse("2022-08-01")))
      assertZIO(result.map(_.data.size).orDie)(Assertion.equalTo(1))
    },
    test("Test Trend Moovable with improper date") {
      val invalidDate = "2022-31-12"
      for {
        trend <- MoovableRepo.trend(moovable.name, invalidDate, hide_data = true).exit
      } yield assert(trend)(fails(startsWithString(s"Text '${invalidDate}' could not be parsed:")))
    },
    test("Test Trend Moovable with 1 data point") {
      for {
        retrieved_moovable <- MoovableRepo.get(moovable.name, hide_data = true)
        trend <- MoovableRepo.trend(moovable.name, "2022-12-31", hide_data = true)
      } yield assert(trend)(Assertion.equalTo((retrieved_moovable, Trend.empty)))
    },
    test("Test Trend Moovable with more data points") {
      MoovableRepo.get(moovable2.name, true)
        .flatMap(retrieved_moovable =>
          MoovableRepo.trend(moovable2.name, "2022-01-05", hide_data = true)
            .map(trend => assert(trend)(Assertion.equalTo((retrieved_moovable, Trend(20.0, 40.0, 10.0)))))
        )
    }
  ).provideLayerShared(inmemory.InmemoryMoovableRepo.live) @@ sequential @@ timed
}

package dev.ga.mooving.moovable.calculations

import dev.ga.mooving.moovable.{Data, Moovable, Trend}
import zio.test._
import zio.test.junit.JUnitRunnableSpec
import zio.test.TestAspect._

import java.time.LocalDate

object TrendServiceSpec extends JUnitRunnableSpec {
  val trendService: TrendService = new TrendImpl()
  val moovable_no_data: Moovable = Moovable(
    name = "test",
    `type` = "test-bike",
    seq = 1,
    data = List[Data]()
  )

  def spec: Spec[Live with Annotations, String] = suite("TrendService Suite")(
    test("Test Moovable with no data") {
      assertZIO(trendService.calculate(moovable_no_data, "2022-12-31"))(Assertion.equalTo(Trend.empty))
    },

    test("Test Moovable with one data point") {
      val moovable_one_data_point: Moovable = moovable_no_data.copy(data =
        List[Data](
          Data(date=LocalDate.parse("2022-01-03"), value=42)
        )
      )
      assertZIO(trendService.calculate(moovable_one_data_point, "2022-12-31"))(Assertion.equalTo(Trend.empty))
    },

    test("Test Moovable with multiple data points") {
      val moovable: Moovable = moovable_no_data.copy(data =
        List[Data](
          Data(date=LocalDate.parse("2022-01-03"), value=10),
          Data(date=LocalDate.parse("2022-01-02"), value=5),
          Data(date=LocalDate.parse("2022-01-01"), value=0)
        )
      )
      assertZIO(trendService.calculate(moovable, "2022-01-05"))(Assertion.equalTo(Trend(10.0, 20.0, 5.0)))
    }
  ) @@ parallel @@ timed
}
